package demoGuru.SeleniumPrep;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;



/**
 * Unit test for simple App.
 */
public class AppTest {
 
   @Test
    public void testApp()
    
    {
	   
	 String path= System.getProperty("user.dir");
	 System.out.println(path);
	  System.setProperty("webdriver.chrome.driver","chromedriver.exe"); 
	  
   WebDriver driver=new HtmlUnitDriver();
   	driver.manage().timeouts().implicitlyWait(15000, TimeUnit.MILLISECONDS);
   
   	driver.manage().window().maximize();
   	driver.manage().deleteAllCookies();
   	driver.get("https://www.google.com/");
   	System.out.println(driver.getCurrentUrl());
   	System.out.println(driver.getTitle());
      
    }
}
