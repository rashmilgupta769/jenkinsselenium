package demoGuru.SeleniumPrep;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;



/**
 * Hello world!
 *
 */
public class App 
{
	
    public static void main( String[] args ) throws InterruptedException
    {
    	System.setProperty("webdriver.chrome.driver","C:\\Users\\Info\\Downloads\\chromedriver_win32\\chromedriver.exe");
    	Map<String, Object> prefs=new HashMap<String, Object>();
    	prefs.put("profile.default_content_settings.popups", 0);
    	prefs.put("download.default_directory", "C:\\Users\\Info\\Downloads");
    	prefs.put("safebrowsing.enabled", "true");
    	ChromeOptions options=new ChromeOptions();
    	options.setExperimentalOption("prefs", prefs);
    	WebDriver driver=new ChromeDriver(options);
    	driver.manage().timeouts().implicitlyWait(15000, TimeUnit.MILLISECONDS);
    	driver.manage().timeouts().pageLoadTimeout(50000, TimeUnit.MILLISECONDS);
    	driver.manage().window().maximize();
    	driver.manage().deleteAllCookies();
    	driver.get("http://demo.guru99.com/test/guru99home/");
    	System.out.println(driver.getCurrentUrl());
    	System.out.println(driver.getTitle());
    	Actions act=new Actions(driver);
    	act.moveToElement(driver.findElement(By.xpath("//li[@class='dropdown']/a[@class='dropdown-toggle']"))).click().build().perform();
    	driver.findElement(By.xpath("//a[text()='Radio & Checkbox Demo']")).click();
    	List<WebElement> radios=driver.findElements(By.xpath("//input[@type='radio']"));
    	System.out.println(radios.get(2).getAttribute("type"));
    	System.out.println(radios.get(2).getSize().getHeight());
    	System.out.println(radios.get(2).getSize().getWidth());
    	System.out.println(radios.get(2).getLocation().getX());
    	System.out.println(radios.get(2).getLocation().getY());
    	Thread.sleep(3000);
    	driver.findElement(By.id("vfb-7-1")).click();
    	Thread.sleep(1000);
    	driver.navigate().refresh();
    	if(driver.findElement(By.id("vfb-7-1")).isSelected()){
    		System.out.println("selected");
    		driver.findElement(By.id("vfb-7-2")).click();
    		Thread.sleep(3000);
    	}
    	driver.findElement(By.id("vfb-6-0")).click();
    	System.out.println(driver.findElement(By.id("vfb-6-0")).isSelected());
    	driver.navigate().back();
    	act.moveToElement(driver.findElement(By.xpath("//li[@class='dropdown']/a[@class='dropdown-toggle']"))).click().build().perform();
    	driver.findElement(By.xpath("//a[text()='Table Demo']")).click();
    	List<WebElement> elements=driver.findElements(By.xpath("//table//tr/td"));
    	for(WebElement ele:elements) {
    		System.out.print(ele.getText());
    	}
    	driver.navigate().back();
    	act.moveToElement(driver.findElement(By.xpath("//li[@class='dropdown']/a[@class='dropdown-toggle']"))).click().build().perform();
    	driver.findElement(By.xpath("//a[text()='Accessing Link']")).click();
    	List<WebElement> links=driver.findElements(By.xpath("//a[text()='click here']"));
    	links.get(0).click();
    	driver.navigate().back();//form//p[@class='radiobutton'] yes buttoncheck
    	driver.navigate().back();
    	act.moveToElement(driver.findElement(By.xpath("//li[@class='dropdown']/a[@class='dropdown-toggle']"))).click().build().perform();
    	driver.findElement(By.xpath("//a[text()='Ajax Demo']")).click();
    	driver.findElement(By.id("yes")).click();
    	driver.findElement(By.id("buttoncheck")).click();
    	WebDriverWait wait=new WebDriverWait(driver, 6);
    	wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//form//p[@class='radiobutton']"), "Radio button is checked and it's value is Yes"));
    	System.out.println(driver.findElement(By.xpath("//form//p[@class='radiobutton']")).getText());
    	act.moveToElement(driver.findElement(By.xpath("//li[@class='dropdown']/a[@class='dropdown-toggle']"))).click().build().perform();
    	driver.findElement(By.xpath("//a[text()='Delete Customer Form']")).click();
    	driver.findElement(By.name("cusid")).sendKeys("ABC");
    	long start=Calendar.getInstance().getTimeInMillis();
    	driver.findElement(By.name("submit")).click();
    	long end=Calendar.getInstance().getTimeInMillis();
    	System.out.println((end-start)*0.001);
    	start=Calendar.getInstance().getTimeInMillis();
    	driver.switchTo().alert().accept();
    	end=Calendar.getInstance().getTimeInMillis();
    	System.out.println((end-start)*0.001);
    	
    	Alert alert=driver.switchTo().alert();
    
    	
    	System.out.println(alert.getText());
    	alert.accept();
    	
    	
    	driver.navigate().back();
    	driver.navigate().back();
    	driver.navigate().back();
    	/*act.moveToElement(driver.findElement(By.xpath("//li[@class='dropdown']/a[@class='dropdown-toggle']"))).click().build().perform();
    	driver.findElement(By.xpath("//a[text()='Yahoo']")).click();
    	driver.findElement(By.xpath("//a[text()='Download Now']")).click();
    	driver.navigate().back();*/
    	/*act.moveToElement(driver.findElement(By.xpath("//li[@class='dropdown']/a[@class='dropdown-toggle']"))).click().build().perform();
    	driver.findElement(By.xpath("//a[text()='Selenium DatePicker Demo']")).click();
    	
    	driver.findElement(By.name("bdaytime")).sendKeys("05012019");
    	driver.findElement(By.name("bdaytime")).sendKeys(Keys.TAB);
    	driver.findElement(By.name("bdaytime")).sendKeys("0245PM");
    	driver.findElement(By.xpath("//input[@type='submit']")).click();*/
    	/*act.moveToElement(driver.findElement(By.xpath("//li[@class='dropdown']/a[@class='dropdown-toggle']"))).click().build().perform();
    	driver.findElement(By.xpath("//a[text()='Social Icon']")).click();
    	driver.findElement(By.className("facebook")).click();*/
    	/*
    	act.moveToElement(driver.findElement(By.xpath("//li[@class='dropdown']/a[@class='dropdown-toggle']"))).click().build().perform();
    	driver.findElement(By.xpath("//a[text()='Scrollbar Demo']")).click();
    	JavascriptExecutor je=(JavascriptExecutor) driver;
    	je.executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.linkText("VBScript")));*/
    	act.moveToElement(driver.findElement(By.xpath("//li[@class='dropdown']/a[@class='dropdown-toggle']"))).click().build().perform();
    	driver.findElement(By.xpath("//a[text()='Drag and Drop Action']")).click();
    	WebElement From1=driver.findElement(By.xpath("//*[@id='credit2']/a"));	
    	
        
     	//Element(DEBIT SIDE) on which need to drop.		
     	WebElement To1=driver.findElement(By.xpath("//*[@id='bank']/li"));					
      
	//Element(SALES) which need to drag.		
     	WebElement From2=driver.findElement(By.xpath("//*[@id='credit1']/a"));
        
	//Element(CREDIT SIDE) on which need to drop.  		
     	WebElement To2=driver.findElement(By.xpath("//*[@id='loan']/li"));					
     
     	//Element(500) which need to drag.		
        WebElement From3=driver.findElement(By.xpath("//*[@id='fourth']/a"));					
        
        //Element(DEBIT SIDE) on which need to drop.		
     	WebElement To3=driver.findElement(By.xpath("//*[@id='amt7']/li"));					
         
	//Element(500) which need to drag.		
        WebElement From4=driver.findElement(By.xpath("//*[@id='fourth']/a"));					
        
        //Element(CREDIT SIDE) on which need to drop.		
     	WebElement To4=driver.findElement(By.xpath("//*[@id='amt8']/li"));					
      
	//Using Action class for drag and drop.		
     						

	//BANK drag and drop.		
     	act.dragAndDrop(From1, To1).build().perform();
        
	//SALES drag and drop.		
     	act.dragAndDrop(From2, To2).build().perform();
        
	//500 drag and drop debit side.		
     	act.dragAndDrop(From3, To3).build().perform();	
        
	//500 drag and drop credit side. 		
     	act.dragAndDrop(From4, To4).build().perform();		
      
	//Verifying the Perfect! message.		
	if(driver.findElement(By.xpath("//a[contains(text(),'Perfect')]")).isDisplayed())							
     	{		
         	System.out.println("Perfect Displayed !!!");					
     	}
		else
     	{
        	System.out.println("Perfect not Displayed !!!");					
     	}		
    	
    }
}
